exports.m2km = (m) => m * 1.60934;
exports.km2m = (km) => km * 0.621371;
exports.c2f = (c) => (c * 9 / 5) + 32;
exports.f2c = (f) => (f - 32) * 5 / 9;