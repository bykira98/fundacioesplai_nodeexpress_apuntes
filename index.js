// let figuras = require("./figuras");
// let glob = require("glob");
// console.log(figuras.areaReactangulo(2, 3));
// console.log(figuras.areaTriangulo(2, 3));

// glob("**/*.js", null, function (er, files) {
//     if(er)
//     {
//         console.log(er);
//     } 
//     else{
//         files.forEach(el => console.log(el));
//     }
// })
// let conversiones = require("./conversiones");
// console.log(conversiones.m2km(2));
// console.log(conversiones.km2m(2));
// console.log(conversiones.c2f(26));
// console.log(conversiones.f2c(2));

// const fetch = require('node-fetch');

// fetch("https://api.citybik.es/v2/networks/bicing")
// .then(res => res.json())
// .then(datos => datos.network.stations)
// .then(estaciones => estaciones.filter(el => el.free_bikes >= process.argv[2]))
// .then(estacioneslibres => estacioneslibres.forEach(el => console.log(el.name)))
// .catch(err => console.log(err));

const express = require("express");
const app = express();

app.use(express.urlencoded({ extended: false }));

app.get("/conversor", function (req, res) {
    res.render("inicio.ejs");
})
app.post("/conversor", function (req, res) {
    let Result;
    switch (req.body.de) {
        case "millas":
            switch (req.body.a) {
                case "millas":
                    Result = req.body.InputValue;
                    break;
                case "km":
                    Result = req.body.InputValue * 1.609;
                    break;
                case "m":
                    Result = req.body.InputValue * 1609.344;
                    break;
                case "yardas":
                    Result = req.body.InputValue * 1760;
                    break;
            }
            break;
        case "km":
            switch (req.body.a) {
                case "millas":
                    Result = req.body.InputValue / 1.609;
                    break;
                case "km":
                    Result = req.body.InputValue;
                    break;
                case "m":
                    Result = req.body.InputValue * 1000;
                    break;
                case "yardas":
                    Result = req.body.InputValue * 1093.613;
                    break;
            }
            break;
        case "m":
            switch (req.body.a) {
                case "millas":
                    Result = req.body.InputValue / 1609.344;
                    break;
                case "km":
                    Result = req.body.InputValue / 1000;
                    break;
                case "m":
                    Result = req.body.InputValue;
                    break;
                case "yardas":
                    Result = req.body.InputValue * 1.094;
                    break;
            }
            break;
        case "yardas":
            switch (req.body.a) {
                case "millas":
                    Result = req.body.InputValue * 1.609;
                    break;
                case "km":
                    Result = req.body.InputValue / 1093.613;
                    break;
                case "m":
                    Result = req.body.InputValue / 1.094;
                    break;
                case "yardas":
                    Result = req.body.InputValue;
                    break;
            }
            break;
    }
    res.render("inicio.ejs", {Result});
})
app.listen(3001, function () {
    console.log("Escuchando el puerto 3001")
})